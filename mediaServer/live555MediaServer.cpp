/**********
This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the
Free Software Foundation; either version 2.1 of the License, or (at your
option) any later version. (See <http://www.gnu.org/copyleft/lesser.html>.)

This library is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
more details.

You should have received a copy of the GNU Lesser General Public License
along with this library; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
**********/
// Copyright (c) 1996-2015, Live Networks, Inc.  All rights reserved
// LIVE555 Media Server
// main program

#include <BasicUsageEnvironment.hh>
#include "DynamicRTSPServer.hh"
#include "version.hh"
extern "C"{
extern int venc_exit(int n);
extern int venc_init(int resolve);
extern int venc_requestIDR();
extern int venc_getFrame(char* buffer,int *datalen);
extern int venc_rotate(int dir);
extern int zfifo_init(char** handle, unsigned int len);
extern int zfifo_destroy(char* handle);
extern int zfifo_read(char* handle, char* data, unsigned int datalen);
extern int zfifo_readEx(char* handle, char* data, unsigned int datalen);
extern int zfifo_write(char* handle, char* data, unsigned int length);
extern int zfifo_check(char *handle);
extern int zfifo_clear(char* handle);
extern int zfifo_clearA(char* handle);

}
extern char* handle;
//************************************
//for saving video
//************************************
FILE* fp;
int gSaveVideo;
int gSaving;
int count;
void initSaveVideo()
{
	count = 0;
	gSaveVideo = 0;
	gSaving = 0;
	fp = NULL;
}
int fileExists(const char* filename)
{
	FILE* fp=fopen(filename,"r");
	if(fp == NULL){
		return 0;
	}else{
		fclose(fp);
		return 1;
	} 
}
int saveVideo(char* buff,int len)
{
	int ret;
	if(len < 1)return -1;
	count ++;
	if(count == 25){
		count = 0;
		gSaveVideo = fileExists("/dev/savevideo");
		//atoi(getenv("SAVE_VIDEO"));
	}
	if(gSaveVideo){
		if(gSaving){
			ret = fwrite(buff,1,len,fp);
			if(ret != len){
				printf("disk error:write fail!\n");
			}
		}else{
			//TODO:create a new file
			char fileName[100];
			time_t timer;
			struct tm *tblock;
			timer = time(NULL);
			tblock = localtime(&timer);
			sprintf(fileName,"/www/mmc/video/%d%02d%02d%02d%02d%02d.h264",tblock->tm_year+1900,tblock->tm_mon+1,tblock->tm_mday,tblock->tm_hour,tblock->tm_min,tblock->tm_sec);
			printf("%s\n",fileName);
			gSaving = 1;
			fp = fopen(fileName,"wb");
			if(fp == NULL){
				printf("disk error:cant create video file!\n");
			}
			venc_requestIDR();
		}
	}else{
		if(fp != NULL){
			gSaving = 0;
			fclose(fp);
			fp = NULL;
			printf("close file!\n");
		}
	}
	return 0;
}
typedef struct myarg{
	int resolve;
	int rotate;
}myarg;
myarg arg;
void* myproc(void*lparam)
{
	int len;
	char buff[800000];
	myarg* p=(myarg*)lparam;
	venc_init(p->resolve);
	venc_rotate(p->rotate);
	printf("rotate === %d\n",p->rotate);
	initSaveVideo();
	zfifo_init(&handle,1000000);
	while(1){
		venc_getFrame(buff,&len);
		zfifo_write(handle,buff,len);
		saveVideo(buff,len);
	}
	return NULL;
}
void start_camera(int resolve,int rotate)
{
	pthread_t pid;
	if(rotate != 0)rotate = 2;
	arg.resolve = resolve;
	arg.rotate = rotate;
	printf("start = %d\n",arg.rotate);
	pthread_create(&pid,NULL,myproc,&arg);
}
int main(int argc, char** argv) {
  // Begin by setting up our usage environment:
  TaskScheduler* scheduler = BasicTaskScheduler::createNew();
  UsageEnvironment* env = BasicUsageEnvironment::createNew(*scheduler);

  UserAuthenticationDatabase* authDB = NULL;

FILE*fp=fopen("hicat.264","rb");
if(fp == NULL){
fp = fopen("hicat.264","wb");
}
fclose(fp);

zfifo_init(&handle,1000000);
if(argc == 1){
	start_camera(0,0);
}else if(argc == 2){
	start_camera(atoi(argv[1]), 0);
}else if(argc == 3){
	start_camera(atoi(argv[1]), atoi(argv[2]));
}else{
	printf("wrong param!\n");
	exit(0);
}

#ifdef ACCESS_CONTROL
  // To implement client access control to the RTSP server, do the following:
  authDB = new UserAuthenticationDatabase;
  authDB->addUserRecord("username1", "password1"); // replace these with real strings
  // Repeat the above with each <username>, <password> that you wish to allow
  // access to the server.
#endif

  // Create the RTSP server.  Try first with the default port number (554),
  // and then with the alternative port number (8554):
  RTSPServer* rtspServer;
  portNumBits rtspServerPortNum = 554;
  rtspServer = DynamicRTSPServer::createNew(*env, rtspServerPortNum, authDB);
  if (rtspServer == NULL) {
    rtspServerPortNum = 8554;
    rtspServer = DynamicRTSPServer::createNew(*env, rtspServerPortNum, authDB);
  }
  if (rtspServer == NULL) {
    *env << "Failed to create RTSP server: " << env->getResultMsg() << "\n";
    exit(1);
  }

  *env << "LIVE555 Media Server\n";
  *env << "\tversion " << MEDIA_SERVER_VERSION_STRING
       << " (LIVE555 Streaming Media library version "
       << LIVEMEDIA_LIBRARY_VERSION_STRING << ").\n";

  char* urlPrefix = rtspServer->rtspURLPrefix();
  *env << "Play streams from this server using the URL\n\t"
       << urlPrefix << "<filename>\nwhere <filename> is a file present in the current directory.\n";
  *env << "Each file's type is inferred from its name suffix:\n";
  *env << "\t\".264\" => a H.264 Video Elementary Stream file\n";
  *env << "\t\".265\" => a H.265 Video Elementary Stream file\n";
  *env << "\t\".aac\" => an AAC Audio (ADTS format) file\n";
  *env << "\t\".ac3\" => an AC-3 Audio file\n";
  *env << "\t\".amr\" => an AMR Audio file\n";
  *env << "\t\".dv\" => a DV Video file\n";
  *env << "\t\".m4e\" => a MPEG-4 Video Elementary Stream file\n";
  *env << "\t\".mkv\" => a Matroska audio+video+(optional)subtitles file\n";
  *env << "\t\".mp3\" => a MPEG-1 or 2 Audio file\n";
  *env << "\t\".mpg\" => a MPEG-1 or 2 Program Stream (audio+video) file\n";
  *env << "\t\".ogg\" or \".ogv\" or \".opus\" => an Ogg audio and/or video file\n";
  *env << "\t\".ts\" => a MPEG Transport Stream file\n";
  *env << "\t\t(a \".tsx\" index file - if present - provides server 'trick play' support)\n";
  *env << "\t\".vob\" => a VOB (MPEG-2 video with AC-3 audio) file\n";
  *env << "\t\".wav\" => a WAV Audio file\n";
  *env << "\t\".webm\" => a WebM audio(Vorbis)+video(VP8) file\n";
  *env << "See http://www.live555.com/mediaServer/ for additional documentation.\n";

  // Also, attempt to create a HTTP server for RTSP-over-HTTP tunneling.
  // Try first with the default HTTP port (80), and then with the alternative HTTP
  // port numbers (8000 and 8080).

/*  if (rtspServer->setUpTunnelingOverHTTP(80) || rtspServer->setUpTunnelingOverHTTP(8000) || rtspServer->setUpTunnelingOverHTTP(8080)) {
    *env << "(We use port " << rtspServer->httpServerPortNum() << " for optional RTSP-over-HTTP tunneling, or for HTTP live streaming (for indexed Transport Stream files only).)\n";
  } else {
    *env << "(RTSP-over-HTTP tunneling is not available.)\n";
  }
*/
  env->taskScheduler().doEventLoop(); // does not return

  return 0; // only to prevent compiler warning
}
